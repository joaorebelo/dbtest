// put something into the database
/*
document.getElementById("saveButton")
        .addEventListener("click", saveItem);

document.getElementById("getButton")
        .addEventListener("click", getItem);

function saveItem() {
  console.log("save button pressed!");

  // get the key & value from the UI
  var k = document.getElementById("key1").value;
  var v = document.getElementById("value").value;

  console.log("Key: "  + k);
  console.log("Value: " + v);

  // put it in the localStorage
  localStorage.setItem(k, v);
  alert("item saved!");

}

function getItem() {
  console.log("get button pressed!");
  // get something out of the database
  //localStorage.getItem("","");
  var k = document.getElementById("key2");
  var value = localStorage.getItem(k);
  document.getElementById("value2").value = value;
}

*/
// 1. create a global database variable
var db = null;

// add event listeners
document.addEventListener("deviceReady", connectToDatabase);
document.getElementById("saveButton").addEventListener("click", saveButtonPressed);
document.getElementById("showAllButton").addEventListener("click", showAllPressed)

function connectToDatabase() {
  console.log("device is ready - connecting to database");
  // 2. open the database. The code is depends on your platform!
  if (window.cordova.platformId === 'browser') {
    console.log("browser detected...");
    // For browsers, use this syntax:
    //  (nameOfDb, version number, description, db size)
    // By default, set version to 1.0, and size to 2MB
    db = window.openDatabase("cestar", "1.0", "Database for Cestar College app", 2*1024*1024);
  }
  else {
    alert("mobile device detected");
    console.log("mobile device detected!");
    var databaseDetails = {"name":"cestar.db", "location":"default"}
    db = window.sqlitePlugin.openDatabase(databaseDetails);
    console.log("done opening db");
  }

  if (!db) {
    alert("databse not opened!");
    return false;
  }

  // 3. create relevant tables
  db.transaction(createTables)

}

function createTables(transaction) {
  var sql = "CREATE TABLE IF NOT EXISTS employee (id integer PRIMARY KEY AUTOINCREMENT, name text, dept text)"
  //var sql = "CREATE TABLE IF NOT EXISTS employee (name text, dept text)"
  transaction.executeSql(sql, [], createSuccess, createFail)
}

function createSuccess(tx, result) {
  alert("Table created! " + JSON.stringify(tx));
  alert("Result: " + JSON.stringify(result));
}
function createFail(error) {
  alert("Failure while creating table: " + error);
}




function saveButtonPressed(transaction) {
  alert("Save button pressed!");
  console.log("save!!!");
  // get name and department from the user interface
  var n = document.getElementById("name").value;
  var d = document.getElementById("dept").value;

  db.transaction(function (transaction) {
      // save the values to the database
      var sql = "INSERT INTO employee (name, dept) VALUES (?,?)";
      //transaction.execusteSql(sql,[n,d], function(tx,result){}, function(error){})
      transaction.executeSql(sql,[n,d], function(tx,result){
        alert("Insert success: " + JSON.stringify(result));
      }, function(error){
        alert("Insert failed: " + error);
      });
    }
  );

}

function showAllPressed() {
  // clear the user interface
  document.getElementById("dbItems").innerHTML = "";

  db.transaction(function(transaction) {
    transaction.executeSql("SELECT * FROM employee", [],
      function (tx, results) {
        var numRows = results.rows.length;

        for (var i = 0; i < numRows; i++) {

          // to get individual items:
          var item = results.rows.item(i);
          console.log(item);
          console.log(item.name);

          // show it in the user interface
          document.getElementById("dbItems").innerHTML +=
              "<p>Name: " + item.name + "</p>"
            + "<p>Dept: " + item.dept + "</p>"
            + "<p>=======================</p>";
        }

      }, function(error){
        alert("An error occured: " + error)
      });
  });
}

/* DELETE statement
db.transaction(function(txn) {
  txn.executeSql("DROP TABLE employee",[],
      function(tx,results){console.log("Successfully Dropped")},
      function(tx,error){console.log("Could not delete")}
  );
});
*/
